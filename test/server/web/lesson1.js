var Lab = require('lab');
var Code = require('code');
var Config = require('../../../config');
var Hapi = require('hapi');
var HomePlugin = require('../../../server/web/index');


var lab = exports.lab = Lab.script();
var request, server;


lab.beforeEach(function (done) {

    var plugins = [ HomePlugin ];
    server = new Hapi.Server();
    server.connection({ port: Config.get('/port/web') });
    server.views({
        engines: {
            dust: require('hapi-dust')
        },
        path: './shared/templates/pages'
    });
    server.register(plugins, function (err) {

        if (err) {
            return done(err);
        }

        done();
    });
});


lab.experiment('Lesson 1:', function () {

    lab.beforeEach(function (done) {

        request = {
            method: 'GET',
            url: '/lesson1'
        };

        done();
    });


    lab.test('Step 1', function (done) {

        server.inject(request, function (response) {

            Code.expect(response.result).to.match(/Joe/g);
            Code.expect(response.statusCode).to.equal(200);

            done();
        });
    });

    lab.test('Step 2', function (done) {

        server.inject(request, function (response) {

            Code.expect(response.result).to.match(/Dirt/g);
            Code.expect(response.statusCode).to.equal(200);

            done();
        });
    });

    lab.test('Step 3', function (done) {

        server.inject(request, function (response) {

            Code.expect(response.result).to.match(/1234 Any Street/g);
            Code.expect(response.result).to.match(/1075 E 20th St/g);
            Code.expect(response.statusCode).to.equal(200);

            done();
        });
    });

    lab.test('Step 4', function (done) {

        server.inject(request, function (response) {

            Code.expect(response.result).to.match(/fullName does not exist/g);
            Code.expect(response.statusCode).to.equal(200);

            done();
        });
    });
});
//
//lab.experiment('Lesson 2:', function () {
//
//    lab.beforeEach(function (done) {
//
//        request = {
//            method: 'GET',
//            url: '/lesson2?styleId=2'
//        };
//
//        done();
//    });
//
//
//    lab.test('render style name', function (done) {
//
//        server.inject(request, function (response) {
//
//            Code.expect(response.result).to.match(/English-Style India Pale Ale/g);
//            Code.expect(response.statusCode).to.equal(200);
//
//            done();
//        });
//    });
//
//    lab.test('render <ul> of beers with class "js-beer-[index number]"', function (done) {
//
//        server.inject(request, function (response) {
//
//            Code.expect(response.result).to.match(/English-Style India Pale Ale/g);
//            Code.expect(response.statusCode).to.equal(200);
//
//            done();
//        });
//    });
//});
