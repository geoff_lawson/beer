Beer App
========
The Beer Training Application

##Installation

    git clone https://geoff_lawson@bitbucket.org/geoff_lawson/beer.git
    cd beer
    npm install
    bower install
    
##Run Beer App

    npm start

## Reference

Dust Documentation: [http://www.dustjs.com/](http://www.dustjs.com/)

Brewery DB API Documentation: [http://www.brewerydb.com/developers/docs](http://www.brewerydb.com/developers/docs)


