if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseView = require('shared/views/base_view'),
        Beers = require('shared/collections/beers');

    return BaseView.extend({

        el: '#bbv-beer-list',

        initialize: function (options) {
            app.beers = new Beers();

            this.listenTo(app.beers, 'sync', this.render);
        },

        render: function () {
            console.log('beerList render');
        }
    });
});