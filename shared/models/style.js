if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseModel = require('./base_model');

    return BaseModel.extend({

        urlRoot: '/api/style',

        defaults: {
            id: undefined
        },

        initialize: function () {

        },

        parse: function (data) {

            return data;
        }
    });
});
