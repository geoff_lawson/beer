if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseModel = require('./base_model');

    return BaseModel.extend({

        urlRoot: '/api/brewery',

        defaults: {
            id: undefined
        },

        initialize: function () {

        },

        parse: function (data) {

            return data;
        }
    });
});
