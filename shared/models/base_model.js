if (typeof define !== 'function') {
    var define = require('amdefine')(module),
        Config = require('../../config'),
        request = require('request');
}

define(function (require) {
    var Backbone = require('backbone');

    return Backbone.Model.extend({

        sync: function (method, model, options) {
            var url;

            if (this.isServer()) {

                url = [
                    this.urlRoot.replace('/api/', Config.get('/apiUrl')),
                    this.get('id'),
                    '?key=' + Config.get('/apiKey')
                ].join('/');

                request(url, function (error, response, body) {
                    var results = JSON.parse(body);

                    if (error) {
                        options.error(error);
                    } else {
                        options.success(results.data);
                    }
                });

            } else {
                 Backbone.sync(method, model, options);
            }
        },

        isServer: function () {

            try {
                return window ? false : true;
            } catch (e) {
                return true;
            }
        },

        isClient: function () {
            return !this.isServer();
        }
    });
});
