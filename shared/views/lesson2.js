if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseView = require('shared/views/base_view');

    return BaseView.extend({

        el: '#bbv-lesson2'
    });
});