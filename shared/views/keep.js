if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseView = require('shared/views/base_view');

    return BaseView.extend({

        el: '#bbv-app',

        events: {
            'change .js-search-criteria': '_setSearch'
        },

        initialize: function (options) {
            this.search = new Backbone.Model();
            app.beerList = new BeerList();

            this.listenTo(this.search, 'change', this._getBeers);
        },

        _setSearch: function (e) {
            var key = $(e.currentTarget).attr('id'),
                val = $(e.currentTarget).val();

            this.search.set(key, val);
        },

        _getBeers: function () {

            app.beers.fetch({
                reset: true,
                data: this.search.toJSON()
            });

            console.log('getBeers');

        }
    });
});