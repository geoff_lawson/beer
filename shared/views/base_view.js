if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var Backbone = require('backbone');

    return Backbone.View.extend();
});
