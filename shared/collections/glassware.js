if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseCollection = require('./base_collection'),
        Glass = require('../models/glass');

    return BaseCollection.extend({

        url: '/api/glassware',

        model: Glass,

        initialize: function () {


        },

        parse: function (data) {

            return data;
        }
    });
});
