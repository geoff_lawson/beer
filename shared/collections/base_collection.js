if (typeof define !== 'function') {
    var define = require('amdefine')(module),
        Config = require('../../config'),
        request = require('request');
}

define(function (require) {
    var _ = require('underscore'),
        Backbone = require('backbone');

    return Backbone.Collection.extend({

        sync: function (method, model, options) {
            var url;

            if (this.isServer()) {
                if (options.url) {
                    url = Config.get('/apiUrl') + options.url + '?';
                } else {
                    url = this.url.replace('/api/', Config.get('/apiUrl')) + '?';
                }

                _.each(options.data, function(value, key) {
                    if (value.length > 0) {
                        url += '&' + key + '=' + value;
                    }
                });

                url += '&key=' + Config.get('/apiKey');

                request(url, function (error, response, body) {
                    var results = JSON.parse(body);

                    if (error) {
                        options.error(error);
                    } else {
                        if (response.statusCode === 200) {
                            options.success(results.data);
                        } else {
                            results.statusCode = response.statusCode || 500;
                            options.error(results);
                        }
                    }
                });

            } else {
                options.processData = true;

                Backbone.sync(method, model, options);
            }
        },

        isServer: function () {

            try {
                return window ? false : true;
            } catch (e) {
                return true;
            }
        },

        isClient: function () {
            return !this.isServer();
        }
    });
});
