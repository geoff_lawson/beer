if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseCollection = require('./base_collection'),
        Style = require('../models/style');

    return BaseCollection.extend({

        url: '/api/styles',

        model: Style,

        initialize: function () {


        },

        parse: function (data) {

            return data;
        }
    });
});
