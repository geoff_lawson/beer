if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseCollection = require('./base_collection'),
        Brewery = require('../models/brewery');

    return BaseCollection.extend({

        url: '/api/breweries',

        model: Brewery,

        initialize: function () {

            console.log('init');
        },

        parse: function (data) {

            return data;
        }
    });
});
