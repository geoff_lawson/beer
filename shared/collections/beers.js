if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function(require) {
    var BaseCollection = require('./base_collection'),
        Beer = require('../models/beer');

    return BaseCollection.extend({

        url: '/api/beers',

        model: Beer,

        initialize: function () {

        },

        parse: function (data) {

            return data;
        }
    });
});
