    var fs = require('fs'),
        async = require('async'),
        Boom = require('boom'),
        _ = require('underscore'),
        Styles = require('../../shared/collections/styles'),
        Glassware = require('../../shared/collections/glassware'),
        Beers = require('../../shared/collections/beers'),
        Brewery = require('../../shared/models/brewery');

exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/public/{param*}',
        handler: {
            directory: {
                path: 'public'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/shared/{param*}',
        handler: {
            directory: {
                path: 'shared'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, reply) {

             return reply.view('index');
        }
    });

    server.route({
        method: 'GET',
        path: '/lesson1',
        handler: function (request, reply) {
            var data = fs.readFileSync(__dirname + '/../../data/lesson1.json');

            return reply.view('lesson1', JSON.parse(data));
        }
    });

    server.route({
        method: 'GET',
        path: '/lesson2',
        handler: function (request, reply) {

            async.parallel([
                    function (callback) {
                        var snId = 'nHLlnK',
                            brewery = new Brewery({ id: snId });

                        brewery.fetch({
                            success: function (model, response, options) {

                                callback(null, model.toJSON());
                            },
                            error: function (model, response, options) {
                                var error = Boom.create(response['statusCode'], response['errorMessage'], response);

                                callback(error, null);
                            }
                        });
                    },
                    function (callback) {
                        var snId = 'nHLlnK',
                            beers = new Beers();

                        beers.fetch({
                            url: 'brewery/' + snId + '/beers',
                            data: request.query,
                            success: function (collection, response, options) {

                                callback(null, collection.toJSON());
                            },
                            error: function (collection, response, options) {
                                var error = Boom.create(response['statusCode'], response['errorMessage'], response);

                                callback(error, null);
                            }
                        });
                    }
                ],
                function (error, results) {

                    if (error) {
                        return reply.view('errors/500', error);
                    }

                    return reply.view('lesson2', {
                        brewery: results[0],
                        beers: results[1],
                        beerCount: function () {
                            return this.beers.length;
                        }
                    });
                });
        }
    });

    server.route({
        method: 'GET',
        path: '/lesson3',
        handler: function (request, reply) {

            async.parallel([
                    function (callback) {
                        var snId = 'nHLlnK',
                            brewery = new Brewery({ id: snId });

                        brewery.fetch({
                            success: function (model, response, options) {

                                callback(null, model.toJSON());
                            },
                            error: function (model, response, options) {
                                var error = Boom.create(response['statusCode'], response['errorMessage'], response);

                                callback(error, null);
                            }
                        });
                    },
                    function (callback) {
                        var snId = 'nHLlnK',
                            beers = new Beers();

                        beers.fetch({
                            url: 'brewery/' + snId + '/beers',
                            data: request.query,
                            success: function (collection, response, options) {

                                callback(null, collection.toJSON());
                            },
                            error: function (collection, response, options) {
                                var error = Boom.create(response['statusCode'], response['errorMessage'], response);

                                callback(error, null);
                            }
                        });
                    }
                ],
                function (error, results) {

                    if (error) {
                        return reply.view('errors/500', error);
                    }

                    return reply.view('lesson3', {
                        brewery: results[0],
                        beers: results[1],
                        beerCount: function () {
                            return this.beers.length;
                        }
                    });
                });
        }
    });

    next();
};

exports.register.attributes = {
    name: 'web'
};


//    styles.fetch({
//        success: function (collection, response, options) {
//
//            callback(null, collection.toJSON());
//        },
//        error: function (collection, response, options) {
//            var error = Boom.create(response['statusCode'], response['errorMessage'], response);
//
//            callback(error, null);
//        }
//    });
//
//    glassware.fetch({
//        success: function (collection, response, options) {
//
//            callback(null, collection.toJSON());
//        },
//        error: function (collection, response, options) {
//            var error = Boom.create(response['statusCode'], response['errorMessage'], response);
//
//            callback(error, null);
//        }
//    });
