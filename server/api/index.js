var Hoek = require('hoek'),
    Boom = require('boom'),
    Beer = require('../../shared/models/beer'),
    Beers = require('../../shared/collections/beers'),
    Brewery = require('../../shared/models/brewery'),
    Breweries = require('../../shared/collections/breweries');


exports.register = function (server, options, next) {

    options = Hoek.applyToDefaults({ basePath: '/api' }, options);

    server.route({
        method: 'GET',
        path: options.basePath + '/data/{param*}',
        handler: {
            directory: {
                path: 'data'
            }
        }
    });

    server.route({
        method: 'GET',
        path: options.basePath + '/beers',
        handler: function (request, reply) {
            var beers = new Beers();

            beers.fetch({
                data: request.query,
                success: function (collection, response, options) {
                    return reply(collection);
                },
                error: function (collection, response, options) {
                    return reply(Boom.create(response['statusCode'], response['errorMessage'], response));
                }
            });
        }
    });

    server.route({
        method: 'GET',
        path: options.basePath + '/beer/{id}',
        handler: function (request, reply) {
            var beer = new Beer({ id: request.params.id });

            beer.fetch({
                success: function (model, response, options) {
                    return reply(model);
                },
                error: function (model, response, options) {
                    console.log(response['statusCode']) ;

                    return reply(Boom.wrap(response, response['statusCode']));
                }
            });
        }
    });

    server.route({
        method: 'GET',
        path: options.basePath + '/breweries',
        handler: function (request, reply) {
            var breweries = new Breweries();

            breweries.fetch({
                data: request.query,
                success: function (collection, response, options) {
                    return reply(collection);
                },
                error: function (collection, response, options) {
                    return reply(Boom.create(response['statusCode'], response['errorMessage'], response));
                }
            });
        }
    });

    server.route({
        method: 'GET',
        path: options.basePath + '/brewery/{id}',
        handler: function (request, reply) {
            var brewery = new Brewery({ id: request.params.id });

            brewery.fetch({
                success: function (model, response, options) {
                    return reply(model);
                },
                error: function (model, response, options) {
                    console.log(response['statusCode']) ;

                    return reply(Boom.wrap(response, response['statusCode']));
                }
            });
        }
    });

    next();
};

exports.register.attributes = {
    name: 'api'
};
